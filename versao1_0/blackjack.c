#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <locale.h>
#include "Baralho.c"
#include "cabecalho.c"

int mostrar_carta(void);
void cabecalho(void);
void linha(void);
void linha2(void);
void menu_player(void);
void pc_wins(void);
void player_wins(void);

int main() {
	setlocale(LC_ALL, "portuguese");
	char opjogo;												//Variaveis que armazenam as opcoes dos jogadores em relacao ao jogo.
	char nome_jogador1, nome_jogador2;

	int x;
	int jogada, totalj = 0, totalc = 0; 						// caso seja Jogador x Computador
	int total1 = 0, total2 = 0;									// caso seja Jogador x Jogador 	
	
	cabecalho();												//puxa a fun��o cabe�alho, que pinta o nome blackjack.
	
	menu_player();												// mostra o menu de 1 ou 2 jogadores.
	scanf ("%c", &opjogo);  									// scanf que recebe o total de jogadores (max 2)
	getchar();		


	if (opjogo == '1') 											//condi��o 1 (JOGADOR X COMPUTADOR)
	{
		//Vez do usuario.

		opjogo='S'; 											// inicializando a variavel com a condi��o verdadeira do while
		
		system("cls");											//limpando o cmd para ficar bonito
		cabecalho();
		
		
		printf("\t\t\t\t\tVoc� tem que fazer 21 pontos para ganhar...\n");  
		printf("\t\t\t\t\t   Por�m n�o pode passar desse limite\n");
		printf("\t\t\t\t\t    Voc� pode parar ap�s cada jogada\n\n\n");
	
		totalj = 0;												// inicializando com 0 a variavel que receber� os valores do JOGADOR
		
		while ((opjogo=='s') || (opjogo=='S')) 					//la�o que pergunta se o usuario quer fazer a jogada ou nao.
		{
			printf("\t\t\t\t\t       Deseja fazer a jogada?[S/N]\n");
			scanf("%c", &opjogo); 								//recebe se o usuario quer fazer a jogada, ou n�o
			getchar();
			printf("\n");
			
			if ((opjogo=='n') || (opjogo=='N'))					//se for um "n/N", o programa para. Se ele quiser continuar...
				break;				
			
			jogada = mostrar_carta();
		
			totalj += jogada; 									// o total do jogador, vai ser o valor que ele tinha at� ent�o, mais o valor aleat�rio.
			
			printf("\t\t\t\t\t\t      PONTOS: %d", totalj); 
			linha(); 											// printa uma linha de "#" (funcao do arquivo 'cabecalho.c')
			
			if (totalj >= 21) 									//se o valor passar dos 21, ele n�o pode pedir mais cartas.
				break;
		}
		
		printf("\t\t\t\t\t\tVOC� MARCOU %d PONTOS", totalj); // mostra o total que o jogador conseguiu juntar e continua pra vez do COMPUTADOR
		linha();
		//Vez do Computador
		
		
		printf("\t\t\t\t\t\t  VEZ DO COMPUTADOR\n\n");			
		
		printf("\t\t\t\t\t      Agora � minha vez de jogar\n\t\t\t\t\t      Vejo que voc� fez %d pontos\n\n", totalj);
		
		totalc = 0; 											//inicializando com 0 a vari�vel que guardar� o total do computador
		
		while (totalc < 21){
			
			srand(time(NULL));									//gera numeros aleatorios de 1 ate 10 de acordo com o tempo do pc
			
			jogada=(rand() % 12)+1; 							//guarda na variavel jogada, um numero entre (0 - 9) + 1;
			
			totalc += jogada; 									//o total do computador, vai ser o valor que ele tinha at� ent�o, mais o valor aleat�rio.
			
			if(totalc >= 21) 
				break;
				
			printf("\t\t\t  Tirei %d e pretendo continuar jogando, pois ainda estou com %d pontos.\n", jogada, totalc); //mostra a carta e o total que ele juntou at� entao
			
			sleep(1); 											//faz o sistema parar por 1 segundo, at� a prox repeti��o do while (para a fun��o srand funcionar)
		}
		
		
		printf("\t\t\t\t\t Tirei %d pontos e fiquei com %d pontos.\n\n\n", jogada, totalc); //mostra a ultima jogada do PC, e o total que ele ficou
		
		printf("\t\t\t\t\t      O COMPUTADOR MARCOU %d PONTOS\n", totalc);
		
		linha();
		
		printf("\t\t\t\t\t\t\tPLACAR\n");
		printf("\t\t\t\t\t     JOGADOR  |%d| x |%d|  COMPUTADOR", totalj, totalc);
		
		linha();
		
		
		//condicionais:	
	
		if (totalc == totalj)  									//se os dois placares for iguais
			printf("Houve um empate...");
			
		else if (totalj == 21) 									//se o jogador fez 21 pontos exatos e o pc nao
		{
			printf("\t\t\t\tO jogador ganhou, fazendo os gloriosos 21 pontos...\n"); 
			player_wins();
		}	
			
		else if (totalc == 21)							 		//se o pc fez 21 pontos exatos e o jogador nao
		{
			printf("\t\t\t\tO COMPUTADOR GANHOU, fazendo os gloriosos 21 pontos...\n"); 
			pc_wins();
		}
		
		else if (totalc > 21 && totalj <= 21) 					//se o pc ultrapassou os 21 pontos, e o jogador n�o ultrapassou
		{
			printf("\t\t\t   O computador tem um n�mero maior de pontos do que � permitido...\n\t\t\t\t\t\tO JOGADOR VENCE\n"); 
			player_wins();
		}	
			
		else if (totalj > 21 && totalc <= 21) 					//se o jogador ultrapassou os 21 pontos, e o pc n�o ultrapassou
		{
			printf("\t\t\t   O jogador tem um n�mero maior de pontos do que � permitido...\nt\t\t\t\t\t\tO COMPUTADOR VENCE\n"); 
			pc_wins();
		}	
			
		else if ((totalc - 21)<(totalj - 21)) 					//se o valor da subtra��o entre o total do computador e 21 for menor que a subtra��o do valor total do jogador e 21
		{
			printf("\t\t\t\t    O computador vence por estar mais perto de 21.\n"); 
			pc_wins();
		}	
		
		else 													//se o valor da subtra��o entre o total do jogador e 21 for menor que a subtra��o do valor total do computador e 21
		{
			printf("\t\t\t\t    O jogador vence por estar mais perto de 21.\n");
			player_wins();
		}	
		
		printf("\n");
	}
	
	else if (opjogo=='2') {
		
		printf("O jogo funciona por turnos, cada jogador tera sua vez e decidira o que fazer...\n");  //print explicando as como ser� o game
		//Vez do jogador numero 1.
		
		opjogo='S';   											// faz com que entre diretamente no while
		
		system("cls");
		
		printf("Voce tem que fazer 21 pontos para ganhar, porem sem estourar esse limite, podendo parar apos cada jogada.\n\n");  
		
		total1 = 0; 											// inicializando a vari�vel do jogador 1
		
		while ((opjogo=='s') || (opjogo=='S')) 					//Laco que deixa as jogadas a criterio do usuario.
		{
			printf("Deseja fazer a jogada?[S/N]\n"); 			// pergunta ao usuario se ele vai jogar
			scanf("%c", &opjogo);  								//scanf que le a resposta
			
			if ((opjogo=='n') || (opjogo=='N'))					//se ele nao quiser jogar, o programa para
			{ 
				break;	
			}
			
			srand(time(NULL));									//Gera numero aleatorios, entre 1 e 13
			
			jogada=(rand() % 12)+1;  							//coloca uma distancia, em que a funcao pode agir
			
			total1 += jogada; 	 								// total do jogador 1 � igual ao valor q ele tinha, mais a carta da rodada atual
			
			printf("\n");
			printf("Voce tirou %d e ate agora marcou %d pontos.\n", jogada, total1);
			printf("\n");
			
			if (total1 >= 21)
			{
				break;
			}
		}
			
		printf("Voce ficou com %d pontos, vamos ver quantos pontos seu adversario fara.\n\n", total1);
		
		
		//Vez do jogador numero 2.
		
		opjogo='S';
		
		system("cls");
		
		printf("Voce tem que fazer 21 pontos para ganhar, porem sem estourar esse limite, podendo parar apos cada jogada.\n");
		printf("Cuidado, pois o jogador 1 marcou %d pontos\n\n", total1);
		
		
		total2=0;
		
		
		while ((opjogo=='s') || (opjogo=='S')) 	//Laco que deixa as jogadas a criterio do usuario.
		{
			printf("Deseja fazer a jogada?[S/N]\n");
			scanf("%c",&opjogo);
			
			if ((opjogo=='n') || (opjogo=='N'))
				break;	
				  				
			srand(time(NULL));				//Gera numeros aleatorios de 1 ate 10.
			jogada=(rand() % 12)+1;
			total2=(total2+jogada);
			
			if (total2 >= 21)
				break;
		
			printf("\n");
			printf("Voce tirou %d e ate agora marcou %d pontos.\n", jogada, total2);
			printf("\n");
		}
			
			printf("\n");
			printf("O jogador 1 terminou com %d pontos e o jogador 2 com %d pontos, portanto...", total1, total1);
			
			if (total1 == total2)
				printf("Houve um empate..."); 
				
			else if (total1 == 21) 
				printf("O Jogador 1 ganhou, fazendo os gloriosos 21 pontos =D...\n"); 
				
			else if (total2 == 21) 
				printf("O Jogador 2 ganhou, fazendo os gloriosos 21 pontos =D...\n"); 
				
			else if (total2 > 21 && total1 <= 21) 
				printf("O Jogador 2 tem um numero maior de pontos do que e permitido... O Jogador 1 vence.\n"); 
				
			else if (total1 > 21 && total2 <= 21) 
				printf("O Jogador 1 tem um numero maior de pontos do que e permitido... O Jogador 2 vence.\n"); 
				
			else if ((21 - total2)>(21 - total1)) 
				printf("O Jogador 2 vence por estar mais perto de 21.\n"); 
				
			else
				printf("O jogador 1 vence por estar mais perto de 21.\n");
				printf("\n"); 
			}
		else 
			printf("O maximo de jogadores permitidos e de 2\n");
			
	return 0;
}	 
  
  
  
  
  
